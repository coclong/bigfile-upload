# bigfile-upload

#### 介绍
大文件秒传、分片上传、断点续传解决方案实现

#### 项目运行

> 为了方便演示使用，本项目使用的是前后端不分离的架构

1. 确认文件上传路径 默认是 F:\tmp 可在 application.yml 中进行修改
2. 连接自己的数据库，导入 sql 目录下面的 db.sql（注意 application.yml 中的数据库名，用户名密码）
3. 分片大小，前后端要对应相同才行（默认是相同的20M不用改）
4. BigfileUploadApplication 是启动类，直接启动即可
5. 启动后访问：http://localhost:8080/page/index.html
