CREATE TABLE `upload_task` (
                               `id` bigint NOT NULL AUTO_INCREMENT,
                               `extend_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL COMMENT '扩展名',
                               `file_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件名称',
                               `file_path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件路径',
                               `identifier` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'md5唯一标识',
                               `upload_status` tinyint NOT NULL DEFAULT '0' COMMENT '上传状态 1成功 0失败',
                               `upload_time` datetime NOT NULL COMMENT '上传时间',
                               `user_id` bigint NOT NULL COMMENT '用户id',
                               `success_time` datetime DEFAULT NULL COMMENT '上传成功时间',
                               PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='上传任务表';

CREATE TABLE `upload_task_detail` (
                                      `id` bigint NOT NULL AUTO_INCREMENT,
                                      `chunk_number` int NOT NULL COMMENT '当前分片数',
                                      `chunk_size` bigint NOT NULL COMMENT '分片大小',
                                      `current_chunk_size` bigint NOT NULL COMMENT '当前分片大小',
                                      `file_path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件路径',
                                      `file_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件名称',
                                      `identifier` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件md5唯一标识',
                                      `relative_path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件相对路径',
                                      `total_chunks` int NOT NULL COMMENT '文件总分片数',
                                      `total_size` bigint NOT NULL COMMENT '文件总大小',
                                      `upload_time` datetime NOT NULL COMMENT '上传时间',
                                      PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='上传任务详情表';

CREATE TABLE `user_file` (
                        `id` bigint NOT NULL AUTO_INCREMENT,
                        `create_time` datetime NOT NULL COMMENT '创建时间',
                        `create_user_id` bigint NOT NULL COMMENT '创建用户id',
                        `file_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件名',
                        `extend_name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL DEFAULT '' COMMENT '扩展名',
                        `file_size` bigint NOT NULL COMMENT '文件大小',
                        `file_path` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件路径',
                        `file_status` tinyint NOT NULL DEFAULT '1' COMMENT '文件状态 0失效 1生效',
                        `file_url` varchar(500) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT '文件url',
                        `identifier` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NOT NULL COMMENT 'md5唯一标识',
                        `storage_type` tinyint NOT NULL DEFAULT '0' COMMENT '存储类型 0-local',
                        `modify_user_id` bigint DEFAULT NULL COMMENT '修改用户id',
                        `delete_flag` tinyint NOT NULL DEFAULT '0' COMMENT '删除标识 0未删除 1已删除',
                        `delete_time` datetime DEFAULT NULL COMMENT '删除时间',
                        PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='文件表';