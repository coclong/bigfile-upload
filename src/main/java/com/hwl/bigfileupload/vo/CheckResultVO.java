package com.hwl.bigfileupload.vo;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class CheckResultVO {
    /**
     * 是否已上传
     */
    private Boolean uploaded;
    /**
     * 访问url
     */
    private String url;
    /**
     * 已上传的切片数
     */
    private List<Integer> uploadedChunks = new ArrayList<>();
}

