package com.hwl.bigfileupload.config.properties;

import com.hwl.bigfileupload.dto.FileChunkDTO;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.io.File;

/**
 * @author sentry
 * @since 2023-09-23
 */
@Data
@Configuration
@ConfigurationProperties(prefix = "upload")
public class UFOPProperties {
    /**
     * 上传模式
     */
    private String mode;
    /**
     * 指定的上传地址
     */
    private String path;
    /**
     * 默认分块大小
     */
    private long chunkSize;

    /**
     * 获取文件访问路径
     */
    public String getFilePath(FileChunkDTO dto) {
        return path + File.separator + dto.getIdentifier() + File.separator + dto.getRelativePath();
    }

    /**
     * 获取文件保存路径
     */
    public String getSavePath(String md5) {
        return path + File.separator + md5;
    }

    public long readyChunkSize(Long chunkSize) {
        if (chunkSize == null || chunkSize <= 0L) {
            return this.chunkSize;
        }
        return chunkSize;
    }
}
