package com.hwl.bigfileupload.config;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.annotation.Configuration;

/**
 * @author sentry
 * @since 2023-09-22
 */
@Configuration
@MapperScan("com.hwl.bigfileupload.mapper")
public class MyBatisPlusConfig {
}
