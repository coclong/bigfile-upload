package com.hwl.bigfileupload.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 上传任务表
 * </p>
 *
 * @author sentry
 * @since 2023-09-23
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("upload_task")
public class UploadTask implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 扩展名
     */
    @TableField("extend_name")
    private String extendName;

    /**
     * 文件名称
     */
    @TableField("file_name")
    private String fileName;

    /**
     * 文件路径
     */
    @TableField("file_path")
    private String filePath;

    /**
     * md5唯一标识
     */
    @TableField("identifier")
    private String identifier;

    /**
     * 上传状态 1成功 0失败
     */
    @TableField("upload_status")
    private Integer uploadStatus;

    /**
     * 上传时间
     */
    @TableField("upload_time")
    private LocalDateTime uploadTime;

    /**
     * 用户id
     */
    @TableField("user_id")
    private Long userId;

    /**
     * 上传成功时间
     */
    @TableField("success_time")
    private LocalDateTime successTime;
}
