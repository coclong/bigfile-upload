package com.hwl.bigfileupload.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 上传任务详情表
 * </p>
 *
 * @author sentry
 * @since 2023-09-23
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("upload_task_detail")
public class UploadTaskDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 当前分片数
     */
    @TableField("chunk_number")
    private Integer chunkNumber;

    /**
     * 分片大小
     */
    @TableField("chunk_size")
    private Long chunkSize;

    /**
     * 当前分片大小
     */
    @TableField("current_chunk_size")
    private Long currentChunkSize;

    /**
     * 文件路径
     */
    @TableField("file_path")
    private String filePath;

    /**
     * 文件名称
     */
    @TableField("file_name")
    private String fileName;

    /**
     * 文件md5唯一标识
     */
    @TableField("identifier")
    private String identifier;

    /**
     * 文件相对路径
     */
    @TableField("relative_path")
    private String relativePath;

    /**
     * 文件总分片数
     */
    @TableField("total_chunks")
    private Integer totalChunks;

    /**
     * 文件总大小
     */
    @TableField("total_size")
    private Long totalSize;

    /**
     * 上传时间
     */
    @TableField("upload_time")
    private LocalDateTime uploadTime;
}
