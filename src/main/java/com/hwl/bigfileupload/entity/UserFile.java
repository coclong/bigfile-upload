package com.hwl.bigfileupload.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * <p>
 * 文件表
 * </p>
 *
 * @author sentry
 * @since 2023-09-23
 */
@Getter
@Setter
@Accessors(chain = true)
@TableName("user_file")
public class UserFile implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    /**
     * 创建时间
     */
    @TableField("create_time")
    private LocalDateTime createTime;

    /**
     * 创建用户id
     */
    @TableField("create_user_id")
    private Long createUserId;

    /**
     * 文件名
     */
    @TableField("file_name")
    private String fileName;

    /**
     * 扩展名
     */
    @TableField("extend_name")
    private String extendName;

    /**
     * 文件大小
     */
    @TableField("file_size")
    private Long fileSize;

    /**
     * 文件路径
     */
    @TableField("file_path")
    private String filePath;

    /**
     * 文件状态 0失效 1生效
     */
    @TableField("file_status")
    private Integer fileStatus;

    /**
     * 文件url
     */
    @TableField("file_url")
    private String fileUrl;

    /**
     * md5唯一标识
     */
    @TableField("identifier")
    private String identifier;

    /**
     * 存储类型 0-local
     */
    @TableField("storage_type")
    private Integer storageType;

    /**
     * 修改用户id
     */
    @TableField("modify_user_id")
    private Long modifyUserId;

    /**
     * 删除标识 0未删除 1已删除
     * 可以拿来做回收站功能
     */
    @TableField("delete_flag")
    private Integer deleteFlag;

    /**
     * 删除时间
     */
    @TableField("delete_time")
    private LocalDateTime deleteTime;
}
