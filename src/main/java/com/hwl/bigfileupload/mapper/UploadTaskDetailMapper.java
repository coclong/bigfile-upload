package com.hwl.bigfileupload.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hwl.bigfileupload.entity.UploadTaskDetail;

/**
 * <p>
 * 上传任务详情表 Mapper 接口
 * </p>
 *
 * @author sentry
 * @since 2023-09-23
 */
public interface UploadTaskDetailMapper extends BaseMapper<UploadTaskDetail> {

}
