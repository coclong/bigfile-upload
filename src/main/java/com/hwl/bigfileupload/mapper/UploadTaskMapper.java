package com.hwl.bigfileupload.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.hwl.bigfileupload.entity.UploadTask;

/**
 * <p>
 * 上传任务表 Mapper 接口
 * </p>
 *
 * @author sentry
 * @since 2023-09-23
 */
public interface UploadTaskMapper extends BaseMapper<UploadTask> {

}
