package com.hwl.bigfileupload;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BigfileUploadApplication {

    public static void main(String[] args) {
        SpringApplication.run(BigfileUploadApplication.class, args);
    }

}
