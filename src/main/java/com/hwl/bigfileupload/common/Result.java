package com.hwl.bigfileupload.common;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Result<T> {

    private final static String SUCCESS = "success";
    private final static int SUCCESS_CODE = 0;
    private final static String FAILED = "failed";
    private final static int FAIL_CODE = 1;

    /**
     * 错误信息，提供给调用者看
     */
    private String msg = "success";
    /**
     * 状态码，暂定 0 1， 后面可根据模块优化
     */
    private int code = 0;

    private boolean success = true;
    /**
     * 反参数据体
     */
    private T data;

    protected Result() {
    }

    public Result(int code) {
        this.code = code;
    }

    public Result(int code, String msg) {
        this(code);
        this.msg = msg;
    }

    public Result(int code, boolean success, String msg) {
        this(code);
        this.msg = msg;
        this.success = success;
    }

    public Result(int code, T data, String msg) {
        this(code, msg);
        this.data = data;
    }

    public static <T> Result<T> ok() {
        return new Result<T>(SUCCESS_CODE, SUCCESS);
    }

    public static <T> Result<T> ok(T data) {
        return new Result<T>(SUCCESS_CODE, data, SUCCESS);
    }

    public static <T> Result<T> error() {
        return new Result<T>(FAIL_CODE, false, FAILED);
    }

    public static <T> Result<T> error(String message) {
        return new Result<T>(FAIL_CODE, false, message);
    }
}
