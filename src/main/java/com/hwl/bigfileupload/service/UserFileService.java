package com.hwl.bigfileupload.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hwl.bigfileupload.dto.FileChunkDTO;
import com.hwl.bigfileupload.entity.UserFile;

/**
 * <p>
 * 文件表 服务类
 * </p>
 *
 * @author sentry
 * @since 2023-09-23
 */
public interface UserFileService extends IService<UserFile> {

    String getFileUrl(FileChunkDTO dto);

}
