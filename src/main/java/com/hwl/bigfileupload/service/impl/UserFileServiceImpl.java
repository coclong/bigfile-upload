package com.hwl.bigfileupload.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.hwl.bigfileupload.config.properties.UFOPProperties;
import com.hwl.bigfileupload.dto.FileChunkDTO;
import com.hwl.bigfileupload.entity.UserFile;
import com.hwl.bigfileupload.mapper.UserFileMapper;
import com.hwl.bigfileupload.service.UserFileService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * <p>
 * 文件表 服务实现类
 * </p>
 *
 * @author sentry
 * @since 2023-09-23
 */
@Service
public class UserFileServiceImpl extends ServiceImpl<UserFileMapper, UserFile> implements UserFileService {

    @Resource
    private UFOPProperties ufopProperties;

    @Override
    public String getFileUrl(FileChunkDTO dto) {
        UserFile userFile = baseMapper.selectOne(Wrappers.<UserFile>lambdaQuery()
                .eq(UserFile::getIdentifier, dto.getIdentifier())
                .last("limit 1"));
        if (userFile == null) return "";

        // 已删除的文件，将这条记录恢复一下
        if (userFile.getDeleteFlag() == 1) {
            baseMapper.updateById(dto.createUserFileEntity().setId(userFile.getId()).setFilePath(ufopProperties.getFilePath(dto)));
        }
        return userFile.getFileUrl();
    }
}
