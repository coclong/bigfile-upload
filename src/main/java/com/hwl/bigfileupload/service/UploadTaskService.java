package com.hwl.bigfileupload.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.hwl.bigfileupload.dto.FileChunkDTO;
import com.hwl.bigfileupload.entity.UploadTask;
import com.hwl.bigfileupload.vo.CheckResultVO;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * <p>
 * 上传任务表 服务类
 * </p>
 *
 * @author sentry
 * @since 2023-09-23
 */
public interface UploadTaskService extends IService<UploadTask> {

    /**
     * 检查文件上传情况
     */
    CheckResultVO checkUpload(FileChunkDTO dto);

    /**
     * 文件上传
     */
    boolean uploadFile(FileChunkDTO dto);

    /**
     * 根据文件md5值下载文件
     */
    void downloadByIdentifier(String identifier, HttpServletRequest request, HttpServletResponse response);
}
